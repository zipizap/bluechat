# bluechat

bluechat - simple cli command to send/receive/receive-process-reply xmpp/jabber chat messages (including gmail chat)



## ?What?

I needed a simple cli-tool to send gmail messages to my android-mobile phone. I wanted to avoid installing more crap in the phone, and noticed Hangouts was already there, so I might as well leverage it

So, I've searched for a cli-tool to use, and found some althout they were not single-file-binary which is easy to copy and "install". Lately I've been passionate with *GO* and decided this could be a good change to refine my programming skills. 

So here it is - bluechat - a no-dependencies single-binary, that enablesto send, receive and make easy-bots from the command-line.

Its not limited to gmail - it works with XMPP/JABBER providers - see the help text

Have fun

NOTE: has nothing to do with other "blue-chat" or bluetooth projects - only the name coincidence *¯\_(ツ)_/¯*





## USAGE


### sendMessage and waitForMessage

This example shows both features - they can be used separately, it was just easier to show them both together

[![bluechat send/receive demo](https://asciinema.org/a/204048.png)](https://asciinema.org/a/204048?autoplay=1)



### receiveAndReply (barebones chat bot)

These examples show how you can easily create a simple bot with bash. You could control your bot from your Hangouts in your own android-phone or android-tablet (comes pre-installed by default in all androids) or via the handouts webpage, which makes it very convenient

NOTE: the bot in the examples is the most-simple-to-show, but dont use it for anything too serious, its just a quick-hack for the demo.

[![bluechat receiveAndReply demo](http://img.youtube.com/vi/zn73SblIKEI/0.jpg)](http://www.youtube.com/watch?v=zn73SblIKEI "bluechat receiveAndReply demo")


### Details and help


    ./bluechat help

		This is a minimalistic xmpp/jabber client, containing only the basic most-usefull operations to send, receive and reply messages from known contacts.
		It is purposedly simple and not a full-fledged chat-client (does not support send/accept invites, files, status changees, etc)
		It will connect and use one xmpp/jabber or gmail-account to communicate - that account login detils must be configured in a .yaml file
		For more detailed info about capabilities and limitations, have a look at https://gitlab.com/zipizap/gmailchatlib - which is the underlying library powering all of it
		
		
		Although it was made and tested with a xxxx@gmail.com account, it works with other xmpp/jabber-providers (ex: https://jabber.hot-chilli.net)
		Try one of: https://list.jabber.at/ and report success/failure in https://gitlab.com/zipizap/gmailchatlib as an issue, so other people known about it. A resume of all reports is kept in https://gitlab.com/zipizap/gmailchatlib/wikis/Other-public-XMPP-Jabber-providers
		
		USAGE
		  bluechat
		  bluechat [command] [flags]
		
		
		AVAILABLE COMMANDS
		  help            Help about any command
		  receiveAndReply Receives a message, executes PROCESSINGCOMMAND and replies back
		  sendMessage     send <TxtMessage> to a known contact
		  waitForMessage  wait untill receives a message from a known contact
		
		Use "bluechat [command] --help" for more information about a command.
		
		
		FLAGS
		  -c, --config string   Config file with auth-info of xmpp/jabber account to use (default "/home/paulo/.bluechat.yaml")
		  -h, --help            help for bluechat
		  -l, --log string      Activate and create log file (warning: may grow fast) (default "none")






## INSTALL AND CONFIGURE

Just download the compiled binary (amd64) and example config file from https://gitlab.com/zipizap/bluechat

    wget 'https://gitlab.com/zipizap/bluechat/raw/master/bluechat'
    wget 'https://gitlab.com/zipizap/bluechat/raw/master/dot_bluechat.yaml'
    chmod +x bluechat
    # edit dot_bluechat.yaml with the login data
    ./bluechat --config ./dot_bluechat.yaml ...


Or if your a go programmer, even better to compile it yourself 


    go get -u gitlab.com/zipizap/bluechat 
    go build gitlab.com/zipizap/bluechat 
    cp $GOPATH/gitlab.com/zipizap/bluechat/dot_bluechat.yaml .
    ./bluechat --config ./dot_bluechat.yaml ...



## COMMENTS, SUGGESTIONS, FEEDBACK

Use gitlab PR



## LICENSE

GPLv3 - see files included
