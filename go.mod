module bluechat

go 1.18

require (
	github.com/fatih/color v1.13.0
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-xmpp v0.0.0-20220712221724-2eb234970ce7
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olebedev/config v0.0.0-20220822221314-86fa169f9f99
	github.com/spf13/cobra v1.6.1
	gitlab.com/zipizap/gmailchatlib v0.0.0-20221107160212-fdef04ba5737
)

require (
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.1.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
