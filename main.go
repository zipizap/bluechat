package main

import (
	"fmt"
	"os"

	"bluechat/cmd"

	"github.com/fatih/color"
)

func main() {
	err := cmd.RootCmd.Execute()
	// NOTE: the returned error will come from the RootCmd.RunE function (if used)
	if err != nil {
		errorColor := color.New(color.FgHiYellow).Add(color.Bold).Add(color.BgRed).SprintFunc()
		fmt.Println(errorColor(fmt.Sprintf("ERROR: %v", err)))
		os.Exit(1)
	}
}
