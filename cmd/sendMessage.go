package cmd

import (
	"fmt"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/spf13/cobra"

	gchat "gitlab.com/zipizap/gmailchatlib"
	//gchat "bluechat/gmailchatlib"
)

// In this file, only minimum comments should be present.
// For more details and explanations, see rootCommand.go
var CmdSendMessage = &cobra.Command{
	Use: "sendMessage --to=MyKnownContact@gmail.com <TxtMessage>",

	Short: "send <TxtMessage> to a known contact",
	Long: `
send <TxtMessage> to contact --to|-t MyKnownContact@gmail.com

The MyKnownContact@gmail.com contact must be known to the configured-yaml account (ie, both the sender and received have already chatted before in some other application like gmail/hangouts webpage/android-app)
NOTE The gmail accounts are limited by google to only send/receive with other gmail accounts. But you can use other xmpp/jabber providers (like https://jabber.hot-chilli.net) in the yaml-config and then contact other xmpp/jabber accounts
For more details, see https://gitlab.com/zipizap/gmailchatlib
`,

	Example: `
bluechat sendMessage --to=MyKnownContact@gmail.com           'This is the message'
bluechat sendMessage --to=MyJabberXmppFriend@hot-chilli.eu   'This is the message sent from a xmpp/jabber (non-gmail) account to another xmpp/jabber (non-gmail) account. Remember gmail only sends/receives to other gmail users'
`,

	// Args indicates the number of expected arguments (does not include $0, (sub)commands, flags)
	// Use one of the following helper-functions:
	//		cobra.ExactArgs(n int)
	//		cobra.MaximumNArgs(n int)
	//		cobra.MinimumNArgs(n int)
	//		cobra.RangeArgs(min int, max int)
	Args: cobra.MinimumNArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		return sendMessage(cmd, args)

		/*
			fmt.Printf("Im command '%s' \n", cmd.Name())
			fmt.Printf("The args are: '%v' \n", args)
			fmt.Printf("The flags are: '%v' \n", cmd.Flags())
			persistFlag_val := cmd.Flag("persistFlag").Value.String()
			subFlag_val := cmd.Flag("subflag").Value.String()
			fmt.Printf("A single flag - persistent or local - can be getted like this: %s, %s \n", persistFlag_val, subFlag_val)
			return nil
		*/
	},
}

func sendMessage(cmd *cobra.Command, args []string) error {
	// init (once)
	err := gchat.Init()
	if err != nil {
		return err
	}

	// setup colors
	color.NoColor = false
	hiblue := color.New(color.FgHiBlue).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()

	receiver_contact := cmd.Flag("to").Value.String()
	message_text := strings.Join(args, " ")

	fmt.Println(yellow(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Sending message..."))
	fmt.Println(yellow(fmt.Sprintf("=============================== %v", time.Now().Local())))
	fmt.Println(hiblue("TO  :"), receiver_contact)
	fmt.Print(hiblue("TEXT:\n") + message_text + "\n\n\n")
	err = gchat.MessageSend(receiver_contact, message_text)
	if err != nil {
		return err
	}
	return nil
}

func init() {
	the_cmd := CmdSendMessage

	// Add flags
	{
		// the_cmd.Flags().StringP("subflag", "s", "defaultValueOfSubflag", "description of subflag")
		the_cmd.Flags().StringP("to", "t", "", "destination contact. Ex: MyKnownContact@gmail.com or MyNonGmailFriend@hot-clilli.eu")
		the_cmd.MarkFlagRequired("to")
	}

	// Now edit rootCmd.go and add this new command with xxx.AddCommand(thisNewCommand)

	//------------------------------------------------------------------------------------------------------
	//-------------- From this point bellow, dont change anything more, its all automatic ------------------
	//------------------------------------------------------------------------------------------------------

	// Apply refinements
	applyRefinementsToCmd(the_cmd)
}
