package cmd

import (
	"fmt"
	"os/exec"
	"time"

	"github.com/fatih/color"
	xmpp "github.com/mattn/go-xmpp"
	"github.com/spf13/cobra"

	gchat "gitlab.com/zipizap/gmailchatlib"
	//gchat "bluechat/gmailchatlib"
)

// In this file, only minimum comments should be present.
// For more details and explanations, see rootCommand.go
var CmdReceiveAndReplyMessage = &cobra.Command{
	Use: "receiveAndReply [-i|--infiniteloop] <PROCESSINGCOMMAND>",

	Short: "Receives a message, executes PROCESSINGCOMMAND and replies back",
	Long: `
Will wait-until-receives a message (from any known contact), and then call PROCESSINGCOMMAND like:
          PROCESSINGCOMMAND <MsgSender> <MsgText>

After PROCESSINGCOMMAND finishes executing, its stdout/stderr will be the reply message

The PROCESSINGCOMMAND can be any script/command (must be executable)
The program will only run once (receive 1 message > call PROCESSINGCOMMAND), but if -i|--infiniteloop is present it will loop infinitely

`,

	Example: `
bluechat receiveAndReplyMessage ./reverse.sh 
bluechat receiveAndReplyMessage /bin/echo

`,

	// Args indicates the number of expected arguments (does not include $0, (sub)commands, flags)
	// Use one of the following helper-functions:
	//		cobra.ExactArgs(n int)
	//		cobra.MaximumNArgs(n int)
	//		cobra.MinimumNArgs(n int)
	//		cobra.RangeArgs(min int, max int)
	Args: cobra.MinimumNArgs(1),

	RunE: func(cmd *cobra.Command, args []string) error {
		return receiveAndReplyMessage(cmd, args)

		/*
			fmt.Printf("Im command '%s' \n", cmd.Name())
			fmt.Printf("The args are: '%v' \n", args)
			fmt.Printf("The flags are: '%v' \n", cmd.Flags())
			persistFlag_val := cmd.Flag("persistFlag").Value.String()
			subFlag_val := cmd.Flag("subflag").Value.String()
			fmt.Printf("A single flag - persistent or local - can be getted like this: %s, %s \n", persistFlag_val, subFlag_val)
			return nil
		*/
	},
}

func receiveAndReplyMessage(cmd *cobra.Command, args []string) error {
	// init (once)
	err := gchat.Init()
	if err != nil {
		return err
	}

	var flagInfiniteloop_val bool
	var processingcommand string
	{
		//flagInfiniteloop_val := cmd.Flag("infiniteloop").Value.
		var err error
		flagInfiniteloop_val, err = cmd.Flags().GetBool("infiniteloop")
		if err != nil {
			return err
		}
		processingcommand = args[0]
	}

	// setup colors
	color.NoColor = false
	hiblue := color.New(color.FgHiBlue).SprintFunc()
	yellow := color.New(color.FgYellow).SprintFunc()

	for {
		// read message (blocks untill a message is received)
		var msgRec_txt string
		var msgRec_chat xmpp.Chat
		var msgRec_sender string
		{
			fmt.Println(yellow("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Waiting to receive next message..."))
			var err error
			msgRec_txt, msgRec_chat, err = gchat.MessageWaitAndReceive()
			if err != nil {
				return err
			}
			msgRec_sender = msgRec_chat.Remote
			fmt.Println(yellow(fmt.Sprintf("=============================== %v", time.Now().Local())))
			fmt.Println(hiblue("SENDER:\t") + msgRec_sender)
			fmt.Println(hiblue("TEXT:\n") + msgRec_txt)
		}

		// call external processingcommand with arg1=msgRec_sender arg2=msgRec_txt
		var processingcommand_stdoutStderr string
		{
			fmt.Println(yellow(".............................. Calling PROCESSINGCOMMAND <MsgSender> <MsgText> ..."))
			fmt.Println(yellow(fmt.Sprintf("=============================== %v", time.Now().Local())))
			cmd := exec.Command(processingcommand, msgRec_sender, msgRec_txt)
			processingcommand_stdoutStderr_bytes, err := cmd.CombinedOutput()
			if err != nil {
				processingcommand_stdoutStderr = "Error: " + err.Error() + "\n"
			} else {
				processingcommand_stdoutStderr = string(processingcommand_stdoutStderr_bytes)
			}
			fmt.Println(hiblue("PROCESSINGCOMMAND :\n")+"'"+processingcommand+"'", "'"+msgRec_sender+"'", "'"+msgRec_txt+"'")
			fmt.Print(hiblue("STDOUTSTDERR:\n") + processingcommand_stdoutStderr)
		}

		// reply to (sender of) a previously-received-chatmessage
		{
			fmt.Println(yellow(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Replying with stdoutStderr of PROCESSINGCOMMAND..."))
			fmt.Println(yellow(fmt.Sprintf("=============================== %v", time.Now().Local())))
			err := gchat.MessageReply(processingcommand_stdoutStderr, msgRec_chat)
			if err != nil {
				return err
			}
			fmt.Println(hiblue("TO  :"), msgRec_sender)
			fmt.Print(hiblue("TEXT:\n") + processingcommand_stdoutStderr + "\n\n\n")
		}

		if !flagInfiniteloop_val {
			break
		}
	}

	return nil
}

func init() {
	the_cmd := CmdReceiveAndReplyMessage

	// Add flags
	{
		// the_cmd.Flags().StringP("subflag", "s", "defaultValueOfSubflag", "description of subflag")
		the_cmd.Flags().BoolP("infiniteloop", "i", false, "Loop indefinitely")
	}

	// Now edit rootCmd.go and add this new command with xxx.AddCommand(thisNewCommand)

	//------------------------------------------------------------------------------------------------------
	//-------------- From this point bellow, dont change anything more, its all automatic ------------------
	//------------------------------------------------------------------------------------------------------

	// Apply refinements
	applyRefinementsToCmd(the_cmd)
}
