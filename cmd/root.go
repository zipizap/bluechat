package cmd

import (
	"errors"
	"log"
	"os"
	"path/filepath"

	"github.com/fatih/color"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"

	gchat "gitlab.com/zipizap/gmailchatlib"
	//gchat "bluechat/gmailchatlib"
)

var DEFAULT_CONFIG_FILE = "/home/User_WillBeInitializedLatter/.bluechat.yaml"

// see https://godoc.org/github.com/spf13/cobra#Command
// Good pratices:
//     ./$0 <on-who>  <do-what>  --how-details-flags <with-whatelse>
//     ./$0 images    list       --extended
//     ./$0 images    add        --extended					 myImage
//		 root subcmd1   subcmd11   flags               args
var RootCmd = &cobra.Command{

	// Use is the command-name and any extra-args necessary
	// A good strategy for this can be:
	//		// in RootCmd
	//		Use: filepath.Base(os.Args[0]),
	//
	//		// in do-nothing/place-holder commands
	//		Use: "mycommandname",
	//
	//		// in do-something/leaf commands, indicate <ARGS> (if any)
	//		Use: "myLeafCommand srcFile dstFile"
	Use: filepath.Base(os.Args[0]),

	// Short is the short description shown in the 'help' output.
	Short: "Minimalistic xmpp chat client compatible with gmail",
	// Long is the long message shown in the 'help <this-command>' output.
	Long: `This is a minimalistic xmpp/jabber client, containing only the basic most-usefull operations to send, receive and reply messages from known contacts. 
It is purposedly simple and not a full-fledged chat-client (does not support send/accept invites, files, status changees, etc)
It will connect and use one xmpp/jabber or gmail-account to communicate - that account login detils must be configured in a .yaml file
For more detailed info about capabilities and limitations, have a look at https://gitlab.com/zipizap/gmailchatlib - which is the underlying library powering all of it

Although it was made and tested with a xxxx@gmail.com account, it works with other xmpp/jabber-providers (ex: https://jabber.hot-chilli.net)
Try one of: https://list.jabber.at/ and report success/failure in https://gitlab.com/zipizap/gmailchatlib as an issue, so other people known about it. A resume of all reports is kept in https://gitlab.com/zipizap/gmailchatlib/wikis/Other-public-XMPP-Jabber-providers

`,

	// Example is a text with examples of how to use the command
	// A good strategy for this can be:
	//		- in RootCmd and also do-nothing/place-holder commands => dont define it
	//		- in do-something/leaf commands => define it
	//Example: "example line1\nexample line2\nexample last line 3\n",

	// Args indicates the number of expected arguments (does not include $0, (sub)commands, flags)
	// Use one of the following helper-functions:
	//		cobra.ExactArgs(n int)
	//		cobra.MaximumNArgs(n int)
	//		cobra.MinimumNArgs(n int)
	//		cobra.RangeArgs(min int, max int)
	// A good strategy for this can be:
	//		- in RootCmd and also do-nothing/place-holder commands => dont define it
	//		- in do-something/leaf commands => define it
	//Args: cobra.ExactArgs(1),

	// Define this only in root command
	Version: "1.3",

	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		// Take care of global flags
		{
			configFlag_val := cmd.Flag("config").Value.String()
			gchat.DEFAULT_YAML_FILE = configFlag_val

			logFlag_val := cmd.Flag("log").Value.String()
			if logFlag_val != "none" {
				gchat.DEFAULT_LOGGER_ENABLED = true     // defaults to false
				gchat.DEFAULT_LOGGER_FILE = logFlag_val // defaults to "./os.Args[0].log"
			}
		}
		return nil
	},

	RunE: func(cmd *cobra.Command, args []string) error {
		// NOTE: the returned error will go back to "err := RootCmd.Execute()"

		// A good strategy for this can be:
		// - in RootCmd (if it should behave like a do-nothing/placeholder) and also do-nothing/place-holder commands use this:
		return errors.New("Missing additional commands")
		//
		// - in do-something/leaf commands (and RootCmd if it should behave like a do-something/leaf-command) use this:
		//  {
		//  	persistFlag_val := cmd.Flag("persistFlag").Value.String()
		//  	fmt.Printf("Im %s with persistFlag '%s' \n", cmd.Name(), persistFlag_val)
		//  	return nil
		//  }
	},
}

func init() {
	the_cmd := RootCmd

	// Add flags
	{
		// Flags can be just-for-this-command (aka local) or global-for-all-commands (aka persistent)
		//
		//	 // in init() of RootCmd.go, add the persistent (global) flags
		//     the_cmd.PersistentFlags().StringP("persistFlag", "p", "persistFlagDefaultVal", "persistFlag description")
		//	 // in init() of other-non-root-commands.go, add the local flags
		//		 the_cmd.Flags().StringP("subflag", "s", "defaultValueOfSubflag", "description of subflag")
		{
			home, err := homedir.Dir()
			if err != nil {
				log.Fatal(err)
			}

			DEFAULT_CONFIG_FILE = home + "/.bluechat.yaml"
		}
		the_cmd.PersistentFlags().StringP("config", "c", DEFAULT_CONFIG_FILE, "Config file with auth-info of xmpp/jabber account to use")

		the_cmd.PersistentFlags().StringP("log", "l", "none", "Activate and create log file (warning: may grow fast)")
		//the_cmd.PersistentFlags().StringP("persistFlag", "p", "persistFlagDefaultVal", "persistFlag description")
	}

	// Add all commands together: root, leaf, placeholder, ... all commands
	// Do this for all commands by editing only in rootCmd.go file
	{
		// Whenever a new command (root, leaf, placeholder) is created, edit this rootCmd.go file and add all of them here
		// Its easier to add all commands in one single place - here - so we see all of them easily (which is ok for the kinds of programs I expect to do for now)
		RootCmd.AddCommand(CmdSendMessage)
		RootCmd.AddCommand(CmdWaitForMessage)
		RootCmd.AddCommand(CmdReceiveAndReplyMessage)
	}
	//------------------------------------------------------------------------------------------------------
	//-------------- From this point bellow, dont change anything more, its all automatic ------------------
	//------------------------------------------------------------------------------------------------------

	// Apply refinements
	applyRefinementsToCmd(the_cmd)
}

// Define this func only in rootcmd.go
// and it will be called from init() of any other-command.go files in same package (dir) as rootcmd.go
func applyRefinementsToCmd(the_cmd *cobra.Command) error {
	// DisableFlagsInUseLine will disable the addition of [flags] to the usage
	// line of a command when printing help or generating docs
	the_cmd.DisableFlagsInUseLine = true

	// UsageTemplate
	//  - Examples in the bottom
	//  - Colors
	var new_usage_template string
	{
		//usageColor := color.New(color.FgHiWhite).Add(color.Bold).Add(color.Underline).SprintFunc()
		usageColor := color.New(color.FgHiWhite).Add(color.Bold).SprintFunc()
		commandColor := color.New(color.FgHiBlue).Add(color.Bold).SprintFunc()
		flagColor := color.New(color.FgHiGreen).Add(color.Bold).SprintFunc()
		exampleTitleColor := color.New(color.FgHiBlack).Add(color.Bold).SprintFunc()
		exampleTextColor := color.New(color.FgHiBlack).SprintFunc()

		new_usage_template = "" + usageColor("USAGE") + `{{if .Runnable}}
  {{.UseLine}}{{end}}{{if .HasAvailableSubCommands}}
  {{.CommandPath}} ` + commandColor("[command]") + " " + flagColor("[flags]") + `{{end}}{{if gt (len .Aliases) 0}}

` + usageColor("ALIASES") + `
  {{.NameAndAliases}}{{end}}{{if .HasAvailableSubCommands}}


` + commandColor("AVAILABLE COMMANDS") + `{{range .Commands}}{{if (or .IsAvailableCommand (eq .Name "help"))}}
  {{rpad .Name .NamePadding }} {{.Short}}{{end}}{{end}}{{end}}{{if .HasAvailableSubCommands}}

Use "{{.CommandPath}} [command] --help" for more information about a command.{{end}}{{if .HasAvailableLocalFlags}}


` + flagColor("FLAGS") + `
{{.LocalFlags.FlagUsages | trimTrailingWhitespaces}}{{end}}{{if .HasAvailableInheritedFlags}}
` + flagColor("GLOBAL FLAGS") + `
{{.InheritedFlags.FlagUsages | trimTrailingWhitespaces}}{{end}}{{if .HasHelpSubCommands}}


Additional help topics:{{range .Commands}}{{if .IsAdditionalHelpTopicCommand}}
    {{rpad .CommandPath .CommandPathPadding}} {{.Short}}{{end}}{{end}}{{end}}{{if .HasExample}}


` + exampleTitleColor("EXAMPLES") + `
` + exampleTextColor("{{.Example}}") + `{{end}}
`
	}
	the_cmd.SetUsageTemplate(new_usage_template)
	return nil
}
