#!/usr/bin/env bash
# "$1" is the remote-contact
# "$2" is the message-text that was received (can be multiline)
# Stdout/stderr of this program will be used to reply back

REMOTE_CONTACT="${1%/*}"
  # "3bx3afblwhgqa3s6h96qpzc6l5@public.talk.google.com"
RECEIVED_MESSAGE="${2}"
ADMIN_CONTACT="3bx3afblwhgqa3s6h96qpzc6l5@public.talk.google.com"

if [[ "${REMOTE_CONTACT}" != "${ADMIN_CONTACT}" ]]; then
  # do nothing and exit
  exit 0
fi

# => The REMOTE_CONTACT is the ADMIN_CONTACT
case "${RECEIVED_MESSAGE}" in
.w)
  w
  ;;
.free)
  free -h
  ;;
.df)
  df -h
  ;;
.reboot)
  echo "No way!"
  ;;
!*)
  # run any command! Be carefull!
  ${RECEIVED_MESSAGE#!}
  exit $?
  ;;
esac

exit 0
