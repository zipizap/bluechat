#!/usr/bin/env bash
# "$1" is the remote-contact
# "$2" is the message-text that was received (can be multiline)
# Stdout/stderr of this program will be used to reply back
echo "[$(hostname)]  Current date here is $(date)"
